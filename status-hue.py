import json
import urllib2

hubIP = "IP_of_Hue_hub"
hubUserID = "userId_from_ /api POST request"
# https://developers.meethue.com/develop/get-started-2/

# The number of Hue groups -- hardcoded for now because my json traversal
# game is weak and groups aren't returned as an array.
groupCount = 5

# Pull list of all groups and lights
groupsStr = urllib2.urlopen("http://" + hubIP + "/api/" + hubUserID + "/groups").read()
lightsStr = urllib2.urlopen("http://" + hubIP + "/api/" + hubUserID + "/lights").read()

# Load into json objects
groups = json.loads(groupsStr)
lights = json.loads(lightsStr)

# Loop through groups showing group name and each light in group
for groupIndex in range(1, (groupCount + 1)):
  print(groups[str(groupIndex)]["name"])
  for lightIndex in range(0, len(groups[str(groupIndex)]["lights"])):
    print("  " + lights[str(groups[str(groupIndex)]["lights"][lightIndex])]["name"] + ", " + ("ON" if lights[str(groups[str(groupIndex)]["lights"][lightIndex])]["state"]["on"] else "off"))
