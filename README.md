# smart-home-reporters

Simple scripts to retrieve status of home. Read only

## TODO:
* Access and write Google/smart accessors (thermo+protect)
* Figure out how to determine internet speed test (since eero lacks API)
  * https://github.com/343max/eero-client
  * https://github.com/jrlucier/eero_tracker
* Figure out how to create reliable garage door status
* Create house overlay and way to dynamically load data